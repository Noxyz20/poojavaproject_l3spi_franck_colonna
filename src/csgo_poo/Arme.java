package csgo_poo;

public class Arme {

    public enum armeMarket {
        Ak47("Ak-47", 2700, 70, 300, 30, 90, 150),
        USP("Usp", 300, 40, 110, 300, 6, 20),
        GLOCK("Glock-18", 300, 40, 110, 300, 10, 30),
        MAA4("M4A4", 3100, 70, 190, 30, 90, 200),
        AWP("AWP", 4700, 300, 700, 5, 10, 100);

        private String weaponName;
        private int price;
        private int dmgBody;
        private int dmgHead;
        private int killReward;
        private int magazine;
        private int munition;

        armeMarket(String weaponName, int price, int dmgBody, int dmgHead, int killReward, int magazine, int munition) {
            this.weaponName = weaponName;
            this.price = price;
            this.dmgBody = dmgBody;
            this.dmgHead = dmgHead;
            this.killReward = killReward;
            this.magazine = magazine;
            this.munition = munition;
        }
    }

    public armeMarket armeUtilise;
    private int magazineActual;
    private int munitionActual;
    public Arme(armeMarket armeSelected){
        this.armeUtilise = armeSelected;
        this.magazineActual = armeUtilise.magazine;
        this.munitionActual = armeUtilise.munition;
    }
    public String getArmeName(){
        return armeUtilise.weaponName;
    }
    public int getArmePrice(){
        return armeUtilise.price;
    }
    public int getArmeDmgHead(){
        return armeUtilise.dmgHead;
    }
    public int getArmeDmgBody(){
        return armeUtilise.dmgBody;
    }
    public int getArmeReward(){
        return armeUtilise.killReward;
    }
    public String getArmeMunition(){
        return armeUtilise.magazine +"/"+armeUtilise.munition;
    }

}

