package csgo_poo;

public class Terroriste extends Player{
    private int plants;

    public Terroriste(String playerName){
        this.playerName = playerName;
        this.armeUse = new Arme(Arme.armeMarket.GLOCK);
    }
    public String displayStat(){
        return "___________\n|"+this.playerName+"\n|Hp: "+this.hp+"\n|Armor: "+this.armor+"\n|Kills: "+this.kills+"\n|Morts: "+this.morts+"\n|Plants: "+this.plants+"\n|Moneys: "+this.money+"\n|+Arme: "+armeUse.getArmeName();
    }
    public void plantBomb(){
        this.money= this.money+300;
        this.plants++;
    }
}
