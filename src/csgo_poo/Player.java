package csgo_poo;

public abstract class Player {
    protected int hp = 100;
    protected int armor = 100;
    protected int kills;
    protected int morts;
    protected int money = 800;
    public String playerName;
    public Arme armeUse;

    public abstract String displayStat();

    public boolean isDie(){
        if (this.hp <= 0) {
            this.morts++;
            return true;
        }else{
            return false;
        }
    }
    public void buyWeapon(Arme.armeMarket weaponToBuy){
        Arme armeSelect = new Arme(weaponToBuy);
        if(this.money >= armeSelect.getArmePrice()){
            this.armeUse =  armeSelect;
            this.money = this.money - armeSelect.getArmePrice();
        }
    }
    public void buyKevlar(){
        this.armor = 100;
        this.money-=600;
    }

    public void shot(Player player, String Location){
        if (Location == "body"){
            if (player.armor > 0){
                player.armor = player.armor - 10;
                player.hp = player.hp - (armeUse.getArmeDmgBody() / 2);
                if(player.isDie()){
                    this.money = this.money + armeUse.getArmeReward();
                    this.kills++;
                }
            }else {
                player.hp = player.hp - armeUse.getArmeDmgBody();
                if(player.isDie()){
                    this.money = this.money + armeUse.getArmeReward();
                    this.kills++;
                }
            }
        }else if (Location == "head"){
            if (player.armor > 0){
                player.armor = player.armor - 10;
                player.hp = player.hp - (armeUse.getArmeDmgHead() / 2);
                if(player.isDie()){
                    this.money = this.money + armeUse.getArmeReward();
                    this.kills++;
                }
            }else {
                player.hp = player.hp - armeUse.getArmeDmgHead();
                if(player.isDie()){
                    this.money = this.money + armeUse.getArmeReward();
                    this.kills++;
                };
            }
        }else{
            System.out.println("its a miss shot");
        }
    }
}
